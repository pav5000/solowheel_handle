
$fa = 1;
$fs = 0.2;

instance();
mirror([1,0,0]) instance();

module instance() {
    translate([3,0,15]) rotate([0,90,0]) adapter();
}

module adapter() {
    difference() {
        union() {
            base_plate();
            difference() {
                translate([4,0,25]) top();
                translate([6,0,0]) difference(){
                    cylinder(r=30, h=30);
                    cylinder(r=17.5, h=31);
                }
            }
        }
        translate([0,0,-1])
            cylinder(d=6, h=20);
        translate([0,0,8.5])
            cylinder(d=16, h=7);
    }
}

module base_plate() {
    bigr=70;
    h=12;
    
    // rotate([0,18.4,0])
    rotate([0,18.4,0]) translate([5,0,0])
        difference() {
            intersection() {
                cylinder(d=34, h=h);
                translate([-17,0,0])
                    cylinder(r=24, h=h);
                translate([-5,0,bigr])
                    sphere(r=bigr);
            }
            scale([1,5,0.1]) translate([0,0,2]) sphere(d=10);
        }
}

module top() {
    h=22;
    difference() {
        translate([2,0,-h/2]) cube([18,35,h], center=true);
        translate([-6,0,-5]) cube([20,15,50], center=true);
        translate([0,20,-5]) rotate([90,0,0]) chamfered();
        translate([0,0,-32]) rotate([0,18.4,0]) cube([50,50,20],center=true);
    }
}

module chamfered(d=4, w=6, l=40) {
    dist=w-d;
    union() {
        translate([dist/2,0,0]) cylinder(d=d,h=l);
        translate([-dist/2,0,0]) cylinder(d=d,h=l);
        translate([0,0,l/2]) cube([dist,d,l],center=true);
    }
}
