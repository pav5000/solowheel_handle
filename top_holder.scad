
$fa = 1;
$fs = 0.2;

width=25; // Ширина
height=40; // Высота проставки

angle = acos(height/130);

rotate([angle,0,0]) v3();

module v3() {
    ending_h=10;
    x=height*height/130;
    h2=sqrt(height*height-x*x);
    translate([0,-8,0]) rotate([0,-90,0]) linear_extrude(height=2, center=true) polygon([[0,0], [0,10], [h2,-x+2], [h2,-x-5], [0,-5]]);
    translate([0,-13,0]) rotate([90-angle,0,0]) {
        difference () {
            union() {
                translate([-width/2,0,0]) cube([width,2,height]);
                translate([-width/2,0,height-4]) cube([width,7,4]);
            }
            
            translate([-5,3.5,height-13/2+8.1]) conescrew(d1=4.7);
            translate([ 5,3.5,height-13/2+8.1]) conescrew(d1=4.7);
            hole_h=15;
            translate([0,0,height-hole_h/2-4]) cube([width-7,15,hole_h], center=true);
            translate([0,0,height-hole_h/2-22]) cube([width-7,15,hole_h], center=true);
        }
    }
    difference() {
        translate([0,-3,1.5]) cube([width,20,3], center=true);
        translate([-5,0,0.1]) conescrew();
        translate([5,0,0.1]) conescrew();
    }
}

module conescrew(d1=4.2, d2=8, headh=3, screwh=10) {
    union() {
        cylinder(d1=d1, d2=d2, h=headh);
        translate([0,0,-screwh]) cylinder(d=d1, screwh+0.1);
    }
}
